<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password')->nullable(false)->change();
            $table->string('first_name')->nullable(false)->change();
            $table->string('last_name')->nullable(false)->change();
            $table->string('middle_name')->nullable(false)->change();

            $table->string('phone')->nullable(false)->change();


            $connection = config('database.default');
            $driver = config("database.connections.{$connection}.driver");
            if ($driver === 'pgsql') {
                DB::statement('CREATE EXTENSION IF NOT EXISTS citext');
                DB::statement('ALTER TABLE users ALTER COLUMN email set not null');
            } else {
                $table->string('email')->nullable(false)->change();
            }

            $table->dropColumn('timezone');
        });
    }

    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password')->nullable()->change();
            $table->string('first_name')->nullable()->change();
            $table->string('last_name')->nullable()->change();
            $table->string('middle_name')->nullable()->change();
            $table->string('phone')->nullable()->change();

            $connection = config('database.default');
            $driver = config("database.connections.{$connection}.driver");

            if ($driver === 'pgsql') {
                DB::statement('CREATE EXTENSION IF NOT EXISTS citext');
                DB::statement('ALTER TABLE users ALTER COLUMN email drop not null');
            } else {
                $table->string('email')->nullable()->change();
            }

            $table->string('timezone')->default('Moscow');
        });
    }
};
