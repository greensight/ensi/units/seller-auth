<?php

namespace App\Http\ApiV1\Modules\Users\Resources;

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Role */
class RolesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'expires' => $this->whenPivotLoaded('role_user', function () {
                return $this->pivot->expires;
            }),
        ];
    }
}
