<?php

namespace App\Http\ApiV1\Modules\Users\Queries;

use App\Domain\Users\Models\Role;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class RolesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Role::query());

        $this->allowedSorts(['id', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('title'),
        ]);

        $this->defaultSort('id');
    }
}
