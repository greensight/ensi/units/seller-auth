<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ExtendedPatchUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::nestedRules('fields', PatchUserRequest::baseRules($this->getRouteId()));
    }

    public function getFields(): array
    {
        return $this->input('fields', []);
    }
}
