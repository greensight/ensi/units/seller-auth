<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class AddRolesToUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'roles' => ['required', 'array'],
            'roles.*' => ['integer', Rule::exists(Role::class, 'id')],
            'expires' => ['nullable', 'date'],
        ];
    }

    public function getRoles(): array
    {
        return $this->input('roles');
    }

    public function getExpires(): ?string
    {
        return $this->input('expires');
    }
}
