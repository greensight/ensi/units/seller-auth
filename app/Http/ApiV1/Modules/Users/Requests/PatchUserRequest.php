<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Http\ApiV1\Support\Rules\StrictEmail;
use Illuminate\Validation\Rule;

class PatchUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::baseRules($this->getRouteId());
    }

    public static function baseRules(int $userId): array
    {
        return [
            'seller_id' => ['sometimes', 'integer'],
            'login' => ['sometimes', Rule::unique(User::class, 'login')->ignore($userId)],
            'password' => ['sometimes', 'string'],
            'active' => ['sometimes', 'boolean'],
            'first_name' => ['sometimes', 'string'],
            'last_name' => ['sometimes', 'string'],
            'middle_name' => ['sometimes', 'string'],
            'phone' => ['sometimes', 'regex:/^\+7\d{10}$/', Rule::unique(User::class, 'phone')->ignore($userId)],
            'email' => ['sometimes', new StrictEmail(), Rule::unique(User::class, 'email')->ignore($userId)],
        ];
    }
}
