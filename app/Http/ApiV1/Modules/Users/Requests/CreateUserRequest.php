<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Http\ApiV1\Support\Rules\StrictEmail;
use Illuminate\Validation\Rule;

class CreateUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'seller_id' => ['required', 'integer'],
            'login' => ['required', Rule::unique(User::class)],
            'password' => ['required', 'string'],
            'active' => ['required', 'boolean'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'middle_name' => ['required', 'string'],
            'phone' => ['required', 'regex:/^\+7\d{10}$/', Rule::unique(User::class)],
            'email' => ['required', new StrictEmail(), Rule::unique(User::class)],
        ];
    }
}
