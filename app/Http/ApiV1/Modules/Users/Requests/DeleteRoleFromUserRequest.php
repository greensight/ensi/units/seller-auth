<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class DeleteRoleFromUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'role_id' => ['required', 'integer', Rule::exists(Role::class, 'id')],
        ];
    }

    public function getRoleId(): int
    {
        return $this->input('role_id');
    }
}
