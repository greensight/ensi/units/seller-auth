<?php

use App\Domain\Users\Models\Role;
use App\Domain\Users\Models\User;
use App\Domain\Users\Models\UserRole;
use App\Http\ApiV1\Modules\Users\Tests\Factories\ExtendedPatchUserRequestFactory;
use App\Http\ApiV1\Modules\Users\Tests\Factories\UserRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/users 201', function () {
    $request = UserRequestFactory::new()->make();

    postJson('/api/v1/users', $request)
        ->assertStatus(201)
        ->assertJsonPath('data.seller_id', $request['seller_id'])
        ->assertJsonPath('data.login', $request['login'])
        ->assertJsonPath('data.active', $request['active'])
        ->assertJsonPath('data.last_name', $request['last_name'])
        ->assertJsonPath('data.first_name', $request['first_name'])
        ->assertJsonPath('data.middle_name', $request['middle_name'])
        ->assertJsonPath('data.email', $request['email'])
        ->assertJsonPath('data.phone', $request['phone']);

    unset($request['password']);
    assertDatabaseHas(User::class, $request);
});

test('POST /api/v1/users check email', function (string $email, int $status = 201) {
    $userData = UserRequestFactory::new()->make([
        'email' => $email,
    ]);

    $response = postJson('/api/v1/users', $userData)
        ->assertStatus($status);

    if ($status == 400) {
        $response->assertJsonPath('errors.0.code', "ValidationError");
    }
})->with([
    ['example@domain.com'],
    ['user.name@domain.net'],
    ['user-name@domain.com'],
    ['user-name12@domain.com'],
    ['user-name@domain.ru'],

    ['user name@domain.com', 400],
    ['user-name@domaincom', 400],
    ['user-name@domain-com', 400],
    ['user-name@[127.0.0.1]', 400],
    ['   @domain.com', 400],
    ['user-name@@domain.com', 400],
    ['user..name@domain.com', 400],
    ['.username@domain.com', 400],
    ['username.@domain.com', 400],
    ['user-name@domain.com-', 400],
    ['user-name@-domain.com', 400],
]);

test('GET /api/v1/users/{id}?include=roles 200', function () {
    /** @var User $user */
    $user = User::factory()->create();
    $userRoles = UserRole::factory()->count(3)->create(['user_id' => $user->id]);

    getJson("/api/v1/users/{$user->id}?include=roles")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $user->id)
        ->assertJsonCount($userRoles->count(), 'data.roles');
});

test('GET /api/v1/users/{id} 404', function () {
    getJson("/api/v1/users/13")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('PATCH /api/v1/users/{id} 200', function () {
    $login = 'test1234';
    $user = User::factory()->create(['seller_id' => 1, 'login' => $login]);
    $id = $user->id;

    $userData = UserRequestFactory::new()
        ->only(['seller_id'])
        ->make(['seller_id' => 2]);

    patchJson("/api/v1/users/$id", $userData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.seller_id', $userData['seller_id'])
        ->assertJsonPath('data.login', $login);

    assertDatabaseHas(User::class, [
        'id' => $id,
        'seller_id' => $userData['seller_id'],
        'login' => $login,
    ]);
});

test('PATCH /api/v1/users/{id}:extended 200', function (int $sellerId, array $filter, int $status) {
    $model = User::factory()->create(['seller_id' => $sellerId]);

    $request = ExtendedPatchUserRequestFactory::new()
        ->withFilter($filter)
        ->make();

    patchJson("/api/v1/users/$model->id:extended", $request)
        ->assertStatus($status);

    if ($status === 200) {
        assertDatabaseHas(User::class, ['id' => $model->id, 'login' => data_get($request, 'fields.login')]);
    }
})->with([
    [1000, [], 200],
    [1000, ['seller_id' => 1000], 200],
    [1000, ['seller_id' => 1001], 404],
]);

test('PATCH /api/v1/users/{id} 404', function () {
    patchJson("/api/v1/users/13", UserRequestFactory::new()->make())
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('DELETE /api/v1/users/{id} success', function () {
    $user = User::factory()->create();

    deleteJson("/api/v1/users/{$user->id}")->assertStatus(200);

    assertModelMissing($user);
});

test('DELETE /api/v1/users/{id} with filter 200', function (int $sellerId, array $filter, int $status) {
    $model = User::factory()->create(['seller_id' => $sellerId]);

    deleteJson("/api/v1/users/{$model->id}", ["filter" => $filter])
        ->assertStatus($status);

    if ($status === 200) {
        assertModelMissing($model);
    } else {
        assertModelExists($model);
    }
})->with([
    [1000, ['seller_id' => 1000], 200],
    [1000, ['seller_id' => 1001], 404],
]);

test('DELETE /api/v1/users/{id} 404', function () {
    deleteJson("/api/v1/users/13")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/users:search 200", function () {
    $users = User::factory()
        ->count(10)
        ->sequence(
            ['active' => true],
            ['active' => false],
        )
        ->create();
    $lastId = $users->last()->id;

    $requestBody = [
        "filter" => [
            "active" => false,
        ],
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/users:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $lastId)
        ->assertJsonPath('data.0.active', false);
});

test("POST /api/v1/users:search filter success", function (
    string $fieldKey,
    mixed $value,
    ?string $filterKey,
    mixed $filterValue,
) {
    /** @var User $user */
    $user = User::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/users:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $user->{$fieldKey}),
    ], 'sort' => ['-id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $user->id);
})->with([
    ['id', null, null, null],
    ['seller_id', null, null, null],
    ['login', null, null, null],
    ['active', null, null, null],
    ['phone', null, null, null],
    ['phone', '+79999999999', 'phone_like', "+799"],
    ['email', null, null, null],
    ['email', 'mail@mail.ru', 'email_like', "mail.ru"],
    ['last_name', 'Фамилия', 'full_name', "Фамил"],
    ['last_name', 'Фамилия', 'last_name_like', "Фам"],
    ['first_name', 'Имя', 'first_name_like', "Им"],
    ['middle_name', 'Отчество', 'middle_name_like', "Отч"],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/users:search sort success", function (string $sort) {
    User::factory()->create();
    postJson("/api/v1/users:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'active', 'created_at', 'updated_at',
]);

test("POST /api/v1/users:search-one 200", function () {
    $user = User::factory()->create(['active' => true]);

    $requestBody = [
        "filter" => [
            "active" => true,
        ],
    ];

    postJson("/api/v1/users:search-one", $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $user->id)
        ->assertJsonPath('data.active', true);
});

test("POST /api/v1/users/{id}:add-roles success", function () {
    $user = User::factory()->create();
    $id = $user->id;

    $role = Role::factory()->create();

    $requestBody = [
        "roles" => [$role->id],
        "expires" => now()->addDay(),
    ];

    postJson("/api/v1/users/$id:add-roles", $requestBody)
        ->assertStatus(200);

    assertDatabaseHas(UserRole::class, [
        'user_id' => $id,
        'role_id' => $role->id,
    ]);
});

test('POST /api/v1/users/{id}:add-roles 400', function () {
    $user = User::factory()->create();
    $id = $user->id;

    $requestBody = [
        "roles" => [13],
        "expires" => now()->addDay(),
    ];

    postJson("/api/v1/users/$id:add-roles", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test('POST /api/v1/users/{id}:add-roles 404', function () {
    $role = Role::factory()->create();

    $requestBody = [
        "roles" => [$role->id],
        "expires" => now()->addDay(),
    ];

    postJson("/api/v1/users/13:add-roles", $requestBody)
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/users/{id}:delete-role 200", function () {
    $user = User::factory()->create();
    $id = $user->id;

    $role = Role::factory()->create();

    UserRole::factory()->create([
        'user_id' => $id,
        'role_id' => $role->id,
    ]);

    $requestBody = [
        "role_id" => $role->id,
    ];

    postJson("/api/v1/users/$id:delete-role", $requestBody)
        ->assertStatus(200);

    assertDatabaseMissing(UserRole::class, [
        'user_id' => $id,
        'role_id' => $role->id,
    ]);
});

test('POST /api/v1/users/{id}:delete-role 400', function () {
    $user = User::factory()->create();
    $id = $user->id;

    $requestBody = [
        "role_id" => 13,
    ];

    postJson("/api/v1/users/$id:delete-role", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test('POST /api/v1/users/{id}:delete-role 404', function () {
    $role = Role::factory()->create();

    $requestBody = [
        "role_id" => $role->id,
    ];

    postJson("/api/v1/users/13:delete-role", $requestBody)
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});
