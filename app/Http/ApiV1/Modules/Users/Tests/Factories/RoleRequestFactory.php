<?php

namespace App\Http\ApiV1\Modules\Users\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class RoleRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'title' => $this->faker->unique()->title(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
