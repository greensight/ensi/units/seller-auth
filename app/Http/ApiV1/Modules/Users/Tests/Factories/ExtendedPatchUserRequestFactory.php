<?php

namespace App\Http\ApiV1\Modules\Users\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\ExtendedPatchRequestFactory;

class ExtendedPatchUserRequestFactory extends ExtendedPatchRequestFactory
{
    protected function getFields(): array
    {
        return UserRequestFactory::new()->make();
    }

    protected function getFilter(): array
    {
        return $this->sellerFilter();
    }
}
