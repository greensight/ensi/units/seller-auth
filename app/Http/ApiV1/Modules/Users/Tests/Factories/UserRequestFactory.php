<?php

namespace App\Http\ApiV1\Modules\Users\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class UserRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'seller_id' => $this->faker->modelId(),
            'login' => $this->faker->unique()->userName(),
            'password' => $this->faker->password(),
            'active' => $this->faker->boolean(),
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->firstName(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
