<?php

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/roles/{id} 200', function () {
    $role = Role::factory()->create();
    $id = $role->id;

    getJson("/api/v1/roles/$id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id);
});

test('GET /api/v1/roles/{id} 404', function () {
    getJson("/api/v1/roles/13")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/roles:search success", function () {
    $roles = Role::factory()
        ->count(5)
        ->create();
    $id = $roles->first()->id;

    $requestBody = [
        "filter" => [
            "id" => $id,
        ],
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/roles:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $id);
});
