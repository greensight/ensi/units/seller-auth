<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Domain\Auth\Actions\LogoutIfAccessDenied;
use App\Domain\Users\Actions\CreateUserAction;
use App\Domain\Users\Actions\DeleteUserAction;
use App\Domain\Users\Actions\PatchUserAction;
use App\Domain\Users\Models\User;
use App\Http\ApiV1\Modules\Users\Queries\UsersQuery;
use App\Http\ApiV1\Modules\Users\Requests\CreateUserRequest;
use App\Http\ApiV1\Modules\Users\Requests\ExtendedPatchUserRequest;
use App\Http\ApiV1\Modules\Users\Requests\PatchUserRequest;
use App\Http\ApiV1\Modules\Users\Resources\UsersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;

class UsersController
{
    public function create(CreateUserRequest $request, CreateUserAction $action): Responsable
    {
        return new UsersResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchUserRequest $request, PatchUserAction $action): Responsable
    {
        /** @var User $user */
        $user = User::query()->findOrFail($id);

        return new UsersResource($action->execute($user, $request->validated()));
    }

    public function extendedPatch(int $id, UsersQuery $query, ExtendedPatchUserRequest $request, PatchUserAction $action): Responsable
    {
        /** @var User $user */
        $user = $query->findOrFail($id);

        return new UsersResource($action->execute($user, $request->getFields()));
    }

    public function delete(int $id, UsersQuery $query, DeleteUserAction $action): Responsable
    {
        $action->execute($id, $query->getEloquentBuilder());

        return new EmptyResource();
    }

    public function get(int $id, UsersQuery $query): Responsable
    {
        return new UsersResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, UsersQuery $query): Responsable
    {
        return UsersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(UsersQuery $query): Responsable
    {
        return new UsersResource($query->firstOrFail());
    }

    public function current(Request $request, LogoutIfAccessDenied $logoutIfAccessDenied): Responsable
    {
        /** @var User|null $user */
        $user = $request->user();

        if ($user) {
            $user->loadMissing('roles');
            $logoutIfAccessDenied->execute($user);

            return new UsersResource($user);
        }

        return new EmptyResource();
    }
}
