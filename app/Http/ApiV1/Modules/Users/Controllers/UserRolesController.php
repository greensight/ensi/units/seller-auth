<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Domain\Users\Actions\AddRolesToUserAction;
use App\Domain\Users\Actions\DeleteRoleFromUserAction;
use App\Http\ApiV1\Modules\Users\Requests\AddRolesToUserRequest;
use App\Http\ApiV1\Modules\Users\Requests\DeleteRoleFromUserRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class UserRolesController
{
    public function add(int $userId, AddRolesToUserRequest $request, AddRolesToUserAction $action): Responsable
    {
        $action->execute($userId, $request->getRoles(), $request->getExpires());

        return new EmptyResource();
    }

    public function delete(int $userId, DeleteRoleFromUserRequest $request, DeleteRoleFromUserAction $action): Responsable
    {
        $action->execute($userId, $request->getRoleId());

        return new EmptyResource();
    }
}
