<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Http\ApiV1\Modules\Users\Queries\RolesQuery;
use App\Http\ApiV1\Modules\Users\Resources\RolesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class RolesController
{
    public function get(int $id, RolesQuery $query): Responsable
    {
        return new RolesResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, RolesQuery $query): Responsable
    {
        return RolesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
