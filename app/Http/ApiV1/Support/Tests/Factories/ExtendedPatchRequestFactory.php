<?php

namespace App\Http\ApiV1\Support\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

abstract class ExtendedPatchRequestFactory extends BaseApiFactory
{
    protected array $fields = [];
    protected ?bool $enrichFields = null;
    protected array $withoutFields = [];

    protected array $filter = [];
    protected ?bool $enrichFilters = null;

    abstract protected function getFields(): array;

    abstract protected function getFilter(): array;

    protected function definition(): array
    {
        return [
            'fields' => $this->getTotalFields(),
            'filter' => $this->getTotalFilter(),
        ];
    }

    private function getTotalFields(): array
    {
        if ($this->enrichFields === null) {
            $fields = $this->deleteKeys($this->getFields(), $this->withoutFields);
        } else {
            $fields = $this->enrichFields ? array_merge($this->getFields(), $this->fields) : $this->fields;
        }

        return $this->deleteKeys($fields, $this->withoutFields);
    }

    private function getTotalFilter(): array
    {
        if ($this->enrichFilters === null) {
            return $this->getFilter();
        }

        return $this->enrichFilters ? array_merge($this->getFilter(), $this->filter) : $this->filter;
    }

    public function withFields(array $fields, bool $enrich = false): self
    {
        $this->fields = $fields;
        $this->enrichFields = $enrich;

        return $this;
    }

    public function withoutFields(array $fieldsNames): self
    {
        $this->withoutFields = $fieldsNames;

        return $this;
    }

    public function withFilter(array $filter, bool $enrich = false): self
    {
        $this->filter = $filter;
        $this->enrichFilters = $enrich;

        return $this;
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    protected function sellerFilter(bool $nullable = false): array
    {
        return [
            'seller_id' => $this->faker->nullable($nullable)->modelId(),
        ];
    }

    private function deleteKeys(array $data, array $keys): array
    {
        foreach ($keys as $key) {
            if (isset($data[$key])) {
                unset($data[$key]);
            }
        }

        return $data;
    }
}
