<?php

namespace App\Http\ApiV1\Support\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class BaseFormRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [];
    }

    public function getRouteId(): int
    {
        return (int)$this->route('id');
    }

    public static function nestedRules(string $prefix, array $rules, bool $required = true): array
    {
        return collect($rules)
            ->mapWithKeys(function ($rules, $key) use ($prefix) {
                $requiredKey = array_search("required", $rules);
                if ($requiredKey !== false) {
                    $rules[$requiredKey] = "required_with:$prefix";
                }

                return ["{$prefix}.{$key}" => $rules];
            })
            ->put($prefix, [$required ? 'required' : 'nullable', 'array'])
            ->all();
    }
}
