<?php

namespace App\Domain\Support\DataBase\Types;

class CitextType extends PostgresType
{
    public function getName(): string
    {
        return 'citext';
    }
}
