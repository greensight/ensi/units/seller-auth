<?php

namespace App\Domain\Support\DataBase\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

abstract class PostgresType extends Type
{
    abstract public function getName(): string;

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $this->getName();
    }

    public function getMappedDatabaseTypes(AbstractPlatform $platform): array
    {
        return match ($platform->getName()) {
            'pgsql', 'postgres', 'postgresql' => [$this->getSQLDeclaration([], $platform)],
            default => [],
        };
    }
}
