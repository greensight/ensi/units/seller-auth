<?php

namespace App\Domain\Auth\Actions;

use App\Domain\Users\Models\User;
use Laravel\Passport\RefreshTokenRepository;
use Laravel\Passport\Token;
use Laravel\Passport\TokenRepository;

class DeleteAuthorisationDataAction
{
    public function __construct(
        protected TokenRepository $tokenRepository,
        protected RefreshTokenRepository $refreshTokenRepository
    ) {
    }

    public function execute(User $user): void
    {
        $tokens = $this->tokenRepository->forUser($user->getAuthIdentifier());

        /** @var Token $token */
        foreach ($tokens as $token) {
            $token->revoke();

            $this->refreshTokenRepository->revokeRefreshTokensByAccessTokenId($token->id);
        }
    }
}
