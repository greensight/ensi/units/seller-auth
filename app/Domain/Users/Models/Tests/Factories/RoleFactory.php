<?php

namespace App\Domain\Users\Models\Tests\Factories;

use App\Domain\Users\Models\Role;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Role>
 */
class RoleFactory extends BaseModelFactory
{
    protected $model = Role::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->numberBetween(1, 1000),
            'title' => $this->faker->unique()->title(),
        ];
    }
}
