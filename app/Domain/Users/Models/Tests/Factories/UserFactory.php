<?php

namespace App\Domain\Users\Models\Tests\Factories;

use App\Domain\Users\Models\User;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<User>
 */
class UserFactory extends BaseModelFactory
{
    protected $model = User::class;

    public function definition(): array
    {
        return [
            'seller_id' => $this->faker->modelId(),
            'login' => $this->faker->unique()->userName(),
            'password' => $this->faker->password(),
            'active' => $this->faker->boolean(),
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->firstName(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail(),
        ];
    }

    public function active($active = true): static
    {
        return $this->state(['active' => $active]);
    }
}
