<?php

namespace App\Domain\Users\Models;

use App\Domain\Users\Models\Tests\Factories\RoleFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int $id
 * @property string $title
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read User $user
 * @property-read UserRole|null $pivot
 */
class Role extends Model
{
    protected $table = 'roles';

    public $incrementing = false;

    public function users(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class)
            ->using(UserRole::class)
            ->withPivot('expires')
            ->withTimestamps();
    }

    public static function factory(): RoleFactory
    {
        return RoleFactory::new();
    }
}
