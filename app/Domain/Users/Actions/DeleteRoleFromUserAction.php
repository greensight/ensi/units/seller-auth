<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\User;

class DeleteRoleFromUserAction
{
    public function execute(int $userId, int $roleId): void
    {
        /** @var User $user */
        $user = User::query()->findOrFail($userId);
        $user->roles()->detach($roleId);
    }
}
