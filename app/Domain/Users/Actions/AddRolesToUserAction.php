<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\User;

class AddRolesToUserAction
{
    public function execute(int $userId, array $roles, ?string $expires): void
    {
        /** @var User $user */
        $user = User::query()->findOrFail($userId);

        $user->roles()->syncWithoutDetaching(collect($roles)->mapWithKeys(function (int $role) use ($expires) {
            return [ $role => [ 'expires' => $expires ] ];
        }));
    }
}
