<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Webmozart\Assert\Assert;

class DeleteUserAction
{
    public function execute(int $id, ?Builder $query = null): void
    {
        $query = $query ?: User::query();
        Assert::isAOf($query->getModel(), User::class);

        $user = $query->findOrFail($id);
        $user->delete();
    }
}
