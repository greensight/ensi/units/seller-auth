<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\User;

class PatchUserAction
{
    public function execute(User $user, array $fields): User
    {
        $user->update($fields);

        return $user;
    }
}
