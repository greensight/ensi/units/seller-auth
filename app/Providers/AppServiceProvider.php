<?php

namespace App\Providers;

use App\Domain\Support\DataBase\Types\CitextType;
use Carbon\CarbonImmutable;
use Doctrine\DBAL\Types\Type;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\ServiceProvider;
use Spatie\QueryBuilder\QueryBuilderRequest;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        if ($this->app->runningUnitTests() || $this->app->runningInConsole()) {
            $this->registerDoctrineTypes();
        }
    }

    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        Date::use(CarbonImmutable::class);
        QueryBuilderRequest::setFilterArrayValueDelimiter('');
    }

    private function registerDoctrineTypes(): void
    {
        $doctrineTypes = [
            CitextType::class,
        ];

        foreach ($doctrineTypes as $doctrineType) {
            /** @var Type $instanceType */
            $instanceType = new $doctrineType();
            if (!$instanceType instanceof Type) {
                continue;
            }

            if (!Type::hasType($instanceType->getName())) {
                Type::addType($instanceType->getName(), $doctrineType);
            }
        }
    }
}
